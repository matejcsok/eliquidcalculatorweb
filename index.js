const Calculate = () => {


    const pg = document.getElementById('PG').value;
    const vg = document.getElementById('VG').value;
    const baseNic = document.getElementById('baseNic').value;
    const amount = document.getElementById('amount').value;
    const arom = document.getElementById('arom').value;
    const weight = document.getElementById('weight').value;
    const claimedVG = document.getElementById('claimedVG').value;


    const nicS = CalculateNic(amount, weight, baseNic, arom);
    const vgS = CalculateVG(vg, amount, claimedVG, weight, baseNic, arom);
    const pgS = CalculatePG(amount, pg, claimedVG, weight, baseNic, arom);
    const aromS = CalculateArom(amount, arom);

    document.getElementById('nicS').innerHTML = nicS;
    document.getElementById('VGS').innerHTML = vgS;
    document.getElementById('PGS').innerHTML = pgS;
    document.getElementById('aromS').innerHTML = aromS;
};

function CalculateArom(amount, arom) {
    return amount / 100 * arom;
}

function CalculateNic(amount, weight, baseNic, arom) {
    return ((amount / (weight / baseNic)) - (CalculateArom(amount, arom) / 3))/100
}

function CalculatePG
(amount, PG, claimedVG, weight, baseNic, arom) {
    return Math.abs((PG / 100) * CalculateNic(amount, weight, baseNic, arom) - amount * ((100 - claimedVG) / 100)) - (CalculateArom(amount, arom) / 3)
}

function CalculateVG(VG, amount, claimedVG, weight, baseNic, arom) {
    return Math.abs((VG / 100) * CalculateNic(amount, weight, baseNic, arom) - amount * ((claimedVG) / 100)) - (CalculateArom(amount, arom) / 3);
}